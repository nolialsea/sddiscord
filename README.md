# Install

- Node v16.x
- `npm install`
- Create a `.env` file and enter those properties:
```
DISCORD_TOKEN=YOUR_DISCORD_BOT_TOKEN
BACKEND_URL=http://localhost:5000/
CHANNEL_NAME=image-generation-sfw;image-generation-nsfw
INFO_CHANNEL_NAME=image-generation-chat
HALL_OF_FAME_CHANNEL_NAME=hall-of-fame
ROLES_TO_ADD_BACKENDS="Administrator;Moderator;Contributor"
ROLES_TO_BATCH="Administrator;Moderator"
```

# Run
- Start the bot with `node index.js`
- Add backends on discord with `!add dream backend BACKEND_URL`