import {Client, Intents, MessageAttachment, MessageEmbed} from "discord.js"
import {config} from "dotenv"
import ImageService from "./ImageService.js"
import {Buffer} from "buffer"
import axios from "axios"
import fs from "fs"
import sharp from "sharp"

config()

const client = new Client({
    intents: [
        Intents.FLAGS.GUILDS,
        Intents.FLAGS.GUILD_MESSAGES,
        Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
        Intents.FLAGS.GUILD_MESSAGE_TYPING,
        Intents.FLAGS.GUILD_MEMBERS,
        Intents.FLAGS.GUILD_BANS,
        Intents.FLAGS.GUILD_VOICE_STATES,
        Intents.FLAGS.DIRECT_MESSAGES,
        Intents.FLAGS.DIRECT_MESSAGE_REACTIONS,
        Intents.FLAGS.DIRECT_MESSAGE_TYPING
    ],
    partials: [
        'CHANNEL', // Required to receive DMs
    ]
})

const imgService = new ImageService(process.env.BACKEND_URL, null, false)
const backends = [] // [{url: process.env.BACKEND_URL, consecutiveFails: 0}]
const emergencyBackends = []
const queue = []
let counter = 0

if (fs.existsSync('./backends.json')) {
    const f = JSON.parse(fs.readFileSync('./backends.json', 'utf-8').toString())
    if (f && f.length > 0) {
        backends.push(...f)
    }
}

if (fs.existsSync('./emergencyBackends.json')) {
    const f = JSON.parse(fs.readFileSync('./emergencyBackends.json', 'utf-8').toString())
    if (f && f.length > 0) {
        emergencyBackends.push(...f)
    }
}

function getInt(text, regex, defaultValue, min, max, idSelector = 1) {
    const n = text.match(regex)?.[idSelector]
    const i = Math.max(min, Math.min(max, parseInt(n)))
    return isNaN(i) ? defaultValue : i
}

function getFloat(text, regex, defaultValue, min, max, idSelector = 1) {
    const n = text.match(regex)?.[idSelector]
    const i = Math.max(min, Math.min(max, parseFloat(n)))
    return isNaN(i) ? defaultValue : i
}

const parsePrompt = (prompt) => {
    if (typeof prompt === "string") {
        const keywords = prompt.match(/{[^}]*}/gi)?.map?.(e => e
            .replace('{', '')
            .replace('}', ''))

        if (keywords) {
            const prompts = []
            const placeholderChoices = {}
            for (let placeholder of keywords) {
                if (!placeholderChoices[placeholder]) placeholderChoices[placeholder] = []
                const choices = placeholder.split('|')
                for (let choice of choices) {
                    prompts.push(prompt.replace(`{${placeholder}}`, choice))
                }
            }

            return [...new Set(parsePrompt(prompts))]
        } else {
            return [prompt]
        }
    } else if (Array.isArray(prompt)) {
        return [...new Set(prompt.map(parsePrompt).flat())]
    }
}


function checkNoBackendAlive(message) {
    if (backends.length === 0) {
        message.reply(`# Oh no, it looks like there are no more backends online right now!`
            + `\nFear not however, you can start a colab yourself using this link and generate a **Backend URL**: https://colab.research.google.com/drive/1PQxlD_mb0uBL86WnvdYgtYOBg70vvKct#scrollTo=eBzHtnS-V6KY`
            + `\nThen use the command \`!add dream backend YOUR_BACKEND_URL_HERE\` to add it to the cluster!`).catch(console.error)
        return true
    }
    return false
}

async function isBackendAlive(backendUrl, timeout = null) {
    try {
        await axios.post(
            backendUrl
            + (backendUrl.endsWith('/') ? '' : `/`)
            + `health_check`, null, timeout !== null ? {timeout} : null)
        return true
    } catch {
        if (timeout !== null)
            return null
        return false
    }
}

async function killBackend(infoChannels, backendUrl) {
    const b = backends.find(b => b.url === backendUrl)
    if (b) {
        backends.splice(backends.indexOf(b), 1)

        for (const ic of infoChannels) {
            await ic.send(`# A backend just died... ${backends.length} backends left!`).catch(console.error);

            if (backends.length === 0) {
                await ic.send(`# Oops, there are no more backends left! Switching on the emergency backends...`
                    + `\n# Please start a colab using this link and generate a new **Backend URL** to add to the cluster! https://colab.research.google.com/drive/1PQxlD_mb0uBL86WnvdYgtYOBg70vvKct#scrollTo=eBzHtnS-V6KY`
                    + `\n# Once the URL is generated, use this command in an image generation channel (not this one): \`!add dream backend YOUR_BACKEND_URL\``).catch(console.error)
            } else if (backends.length === 1) {
                await ic.send(`# Oops, there is only one backend left!`
                    + `\n# Please start a colab using this link and generate a new **Backend URL** to add to the cluster! https://colab.research.google.com/drive/1PQxlD_mb0uBL86WnvdYgtYOBg70vvKct#scrollTo=eBzHtnS-V6KY`
                    + `\n# Once the URL is generated, use this command in an image generation channel (not this one): \`!add dream backend YOUR_BACKEND_URL\``).catch(console.error)
            } else if (backends.length <= 3) {
                await ic.send(`# Backends count low. Please start a colab using this link and generate a new **Backend URL** to add to the cluster! https://colab.research.google.com/drive/1PQxlD_mb0uBL86WnvdYgtYOBg70vvKct#scrollTo=eBzHtnS-V6KY`
                    + `\n# Once the URL is generated, use this command in an image generation channel (not this one): \`!add dream backend YOUR_BACKEND_URL\``).catch(console.error)
            }

        }

        fs.writeFileSync('./backends.json', JSON.stringify(backends, null, 4))
    }
}

async function handleImageGenFailure(infoChannels, message, backendUrl) {
    if (await isBackendAlive(backendUrl)) {
        message.reply(`# Oops, backend couldn't process your image! It might be too big!`).catch(console.error)
    } else {
        const b = backends.find(b => b.url === backendUrl)
        if (!b) {
            message.reply(`# Oops, backend couldn't process your image! I think it died, too!`).catch(console.error)
        } else {
            await message.reply(`# Backend seems dead, removing it from the cluster...`
                + `\n# You can retry your prompt, sorry!`).catch(console.error)

            backends.splice(backends.indexOf(b), 1)

            for (const ic of infoChannels) {
                await ic.send(`# A backend just died... ${backends.length} backends left!`).catch(console.error);

                if (backends.length === 0) {
                    await ic.send(`# Oops, there are no more backends left!`
                        + `\n# Please start a colab using this link and generate a new **Backend URL** to add to the cluster! https://colab.research.google.com/drive/1PQxlD_mb0uBL86WnvdYgtYOBg70vvKct#scrollTo=eBzHtnS-V6KY`
                        + `\n# Once the URL is generated, use this command in an image generation channel (not this one): \`!add dream backend YOUR_BACKEND_URL\``).catch(console.error)
                } else if (backends.length === 1) {
                    await ic.send(`# Oops, there is only one backend left!`
                        + `\n# Please start a colab using this link and generate a new **Backend URL** to add to the cluster! https://colab.research.google.com/drive/1PQxlD_mb0uBL86WnvdYgtYOBg70vvKct#scrollTo=eBzHtnS-V6KY`
                        + `\n# Once the URL is generated, use this command in an image generation channel (not this one): \`!add dream backend YOUR_BACKEND_URL\``).catch(console.error)
                } else if (backends.length <= 3) {
                    await ic.send(`# Backends count low. Please start a colab using this link and generate a new **Backend URL** to add to the cluster! https://colab.research.google.com/drive/1PQxlD_mb0uBL86WnvdYgtYOBg70vvKct#scrollTo=eBzHtnS-V6KY`
                        + `\n# Once the URL is generated, use this command in an image generation channel (not this one): \`!add dream backend YOUR_BACKEND_URL\``).catch(console.error)
                }

            }

            fs.writeFileSync('./backends.json', JSON.stringify(backends, null, 4))
        }
    }
}

async function addBackend(infoChannels, message, url, emergency = false) {
    if (!/^(ftp|http|https):\/\/[^ "]+$/.test(url)) {
        message.reply(`# You must provide a valid URL!`).catch(console.error)
    } else {

        const be = emergency ? emergencyBackends : backends

        if (!be.map(b => b.url).includes(url)) {
            be.push({url, consecutiveFails: 0})
            if (emergency)
                fs.writeFileSync('./emergencyBackends.json', JSON.stringify(backends, null, 4))
            else
                fs.writeFileSync('./backends.json', JSON.stringify(backends, null, 4))

            await message.reply(`# ${message.author} added a new ${emergency ? 'emergency backend' : 'backend'}! ${backends.length} backends and ${emergencyBackends.length} emergency backends currently running!`).catch(console.error)

            for (let ic of infoChannels) {
                await ic.send(`# ${message.author} added a new ${emergency ? 'emergency backend' : 'backend'}! ${backends.length} backends and ${emergencyBackends.length} emergency backends currently running!`).catch(console.error)
            }

            message.delete().catch(console.error)
        } else {
            await message.reply(`# ${emergency ? 'Emergency backend' : 'Backend'} was already registered!`).catch(console.error)
            message.delete().catch(console.error)
        }
    }
}

async function img2img(obj, infoChannels, backendUrl) {
    let prompt
    const isMultipleIterations = !Array.isArray(obj.prompt) && obj.iterations !== null
    if (!Array.isArray(obj.prompt) && obj.iterations !== null && obj.iterations < 1) return
    if (Array.isArray(obj.prompt)) {
        prompt = obj.prompt.pop()
        if (!prompt) return
        queue.push(obj)
    } else {
        prompt = obj.prompt
        if (isMultipleIterations) {
            obj.iterations--
            obj.seed = Math.floor(Math.random() * 1000000)
            if (obj.iterations > 0)
                queue.push(obj)
        }
    }

    let image

    try {
        if (obj.src) {
            image = Buffer
                .from((await axios.get(obj.src, {responseType: 'arraybuffer'})).data, 'binary')
        } else {
            throw new Error()
        }
    } catch {
        obj.message.reply(`# Oops, something happened!`).catch(console.error)
        return
    }

    if (!image) {
        obj.message.reply(`# Image couldn't be loaded!`).catch(console.error)
        return
    }

    image = await sharp(image).resize({width: obj.width, height: obj.height, fit: "inside"}).png().toBuffer()

    const confirmationMsg = await obj.message.reply(`\`\`\`!overdream ${prompt} -s ${obj.steps} -C ${obj.scale} -str ${obj.strength} -S ${obj.seed} -src ${obj.src}\`\`\``).catch(console.error)

    try {
        imgService.backendUrl = encodeURI(backendUrl)

        const timestamp = Date.now()
        const buff = await imgService.generateImageVariation(image, prompt, obj)


        const delta = Date.now() - timestamp
        obj.elapsedTimeInSeconds = (delta / 1000).toFixed(2)

        if (!buff || buff.length < 10000) throw new Error("Couldn't generate image")
        const b = backends.find(b => b.url === backendUrl)
        if (b)
            b.consecutiveFails = 0

        const attachment = new MessageAttachment(buff, 'img.png')

        const embed = new MessageEmbed()
            .setColor('#6666ff')
            .setTitle(`Image generated! (in ${obj.elapsedTimeInSeconds}sec)`)
            .setDescription(`\`\`\`!overdream ${prompt} -s ${obj.steps} -C ${obj.scale} -str ${obj.strength} -S ${obj.seed} -src ${obj.src}\`\`\``)


        if (obj.dmCopy) {
            obj.message.author.send({
                embeds: [embed],
                files: [attachment]
            }).catch(console.error)
        } else {
            obj.message.reply({
                embeds: [embed],
                files: [attachment]
            }).catch(console.error)
        }

        confirmationMsg.delete().catch(console.error)

    } catch (e) {
        console.error(e)

        handleImageGenFailure(infoChannels, obj.message, imgService.backendUrl).catch(console.error)
    }
}

async function txt2img(obj, infoChannels, backendUrl) {
    let prompt
    const isMultipleIterations = !Array.isArray(obj.prompt) && obj.iterations !== null
    if (!Array.isArray(obj.prompt) && obj.iterations !== null && obj.iterations < 1) return
    if (Array.isArray(obj.prompt)) {
        prompt = obj.prompt.pop()
        if (!prompt) return
        queue.push(obj)
    } else {
        prompt = obj.prompt
        if (isMultipleIterations) {
            obj.iterations--
            obj.seed = Math.floor(Math.random() * 1000000)
            if (obj.iterations > 0)
                queue.push(obj)
        }
    }

    const confirmationMsg = await obj.message.reply(`\`\`\`!dream ${prompt} -s ${obj.steps} -W ${obj.width} -H ${obj.height} -C ${obj.scale} -S ${obj.seed}\`\`\``).catch(console.error)

    try {
        imgService.backendUrl = encodeURI(backendUrl)

        const timestamp = Date.now()
        const buff = await imgService.generateImage(prompt, obj)
        if (isMultipleIterations)
            obj.seed = Math.floor(Math.random() * 1000000)

        if (!buff || buff.length < 10000) throw new Error("Couldn't generate image")
        const b = backends.find(b => b.url === backendUrl)
        if (b)
            b.consecutiveFails = 0

        const delta = Date.now() - timestamp
        obj.elapsedTimeInSeconds = (delta / 1000).toFixed(2)

        const embed = new MessageEmbed()
            .setColor('#6666ff')
            .setTitle(`Image generated! (in ${obj.elapsedTimeInSeconds}sec)`)
            .setDescription(`\`\`\`!dream ${prompt} -s ${obj.steps} -W ${obj.width} -H ${obj.height} -C ${obj.scale} -S ${obj.seed}\`\`\``)

        const attachment = new MessageAttachment(buff, 'img.png')

        if (obj.dmCopy) {
            obj.message.author.send({
                embeds: [embed],
                files: [attachment]
            }).catch(console.error)
        } else {
            obj.message.reply({
                embeds: [embed],
                files: [attachment]
            }).catch(console.error)
        }

        confirmationMsg.delete().catch(console.error)


    } catch (e) {
        console.error(e)
        handleImageGenFailure(infoChannels, obj.message, imgService.backendUrl).catch(console.error)
    }
}

async function processQueue(infoChannels) {
    const obj = queue.pop()

    if (!obj) return setTimeout(() => {
        processQueue(infoChannels)
    }, 100)

    // Find an available backend
    let backendUrl

    if (backends.length === 0) {
        backendUrl = emergencyBackends[++counter % emergencyBackends.length].url

        while (!await isBackendAlive(backendUrl, 1000)) {
            backendUrl = emergencyBackends[++counter % emergencyBackends.length].url
        }
    } else {
        backendUrl = backends[++counter % backends.length].url

        while (!await isBackendAlive(backendUrl, 1000)) {
            backendUrl = backends[++counter % backends.length].url
        }
    }

    // Execute related request
    if (obj.type === "img2img") {
        img2img(obj, infoChannels, backendUrl).catch(console.error)
    } else if (obj.type === "txt2img") {
        txt2img(obj, infoChannels, backendUrl).catch(console.error)
    }

    setTimeout(() => {
        processQueue(infoChannels)
    }, 100)
}

function isAuthorizedChannel(channel) {
    return process.env.CHANNEL_NAME.split(';').includes(channel?.name?.toLowerCase?.()) || isChannelNSFW(channel)
}

function isChannelNSFW(channel) {
    return process.env.CHANNEL_NAME_NSFW.split(';').includes(channel?.name?.toLowerCase?.())
}

client.on("ready", async () => {
    const guilds = await client.guilds.fetch()
    const infoChannels = []
    const hallOfFameChannels = []
    const hallOfFameNSFWChannels = []
    for (let g of guilds.values()) {
        const guild = await g.fetch()
        const infoChannel = await (await guild.channels.fetch()).find(c => c.name === process.env.INFO_CHANNEL_NAME).fetch()
        infoChannels.push(infoChannel)
        const hallOfFameChannel = await (await guild.channels.fetch()).find(c => c.name === process.env.HALL_OF_FAME_CHANNEL_NAME).fetch()
        hallOfFameChannels.push(hallOfFameChannel)
        const hallOfFameNSFWChannel = await (await guild.channels.fetch()).find(c => c.name === process.env.HALL_OF_FAME_CHANNEL_NAME_NSFW).fetch()
        hallOfFameNSFWChannels.push(hallOfFameNSFWChannel)
    }

    console.log(infoChannels && infoChannels.length > 0 ? 'Info channel found' : 'Info channel not found')

    client.on("messageReactionAdd", async (reaction, u) => {
        if (!isAuthorizedChannel(reaction.message?.channel)) return

        if (reaction?.message?.attachments?.first?.()?.url && reaction.count === 3) {
            for (let hofc of isChannelNSFW(reaction?.message?.channel) ? hallOfFameNSFWChannels : hallOfFameChannels) {
                hofc.send(`Author: ${u} (${u.username})\nReaction: ${reaction.emoji}\nImage: ${reaction?.message?.attachments?.first?.().url}`).catch(console.error)
            }
        }
    })

    client.on("messageCreate", async (message) => {
        if (!isAuthorizedChannel(message.channel)) return

        const checkBackendCommand = message.cleanContent.match(/^!check ?dream ?ba?c?k?e?n?d?s?/i)
        if (checkBackendCommand) {
            message.reply(`# There are currently ${backends.length} backends running and ${emergencyBackends.length} emergency backends!`).catch(console.error)
            return
        }

        const addBackendCommand = message.cleanContent.match(/^!add ?dream ?ba?c?k?e?n?d? ?([^\n]*)/i)
        if (addBackendCommand) {
            if (!message.member?.roles?.cache
                ?.find(r =>
                    process.env.ROLES_TO_ADD_BACKENDS.split(';')
                        .includes(r.name)
                )) return message.reply(`# You don't have the proper role for that command!`)
            const url = addBackendCommand[1].trim()

            await addBackend(infoChannels, message, url)
            return
        }

        const addEmergencyBackendCommand = message.cleanContent.match(/^!add ?dream ?ba?c?k?e?n?d? ?em?e?r?g?e?n?c?y? ?([^\n]*)/i)
        if (addEmergencyBackendCommand) {
            if (!message.member?.roles?.cache
                ?.find(r =>
                    process.env.ROLES_TO_ADD_BACKENDS.split(';')
                        .includes(r.name)
                )) return message.reply(`# You don't have the proper role for that command!`)
            const url = addEmergencyBackendCommand[1].trim()

            await addBackend(infoChannels, message, url, true)
            return
        }

        const canBatch = message.member?.roles?.cache
            ?.find(r =>
                process.env.ROLES_TO_BATCH.split(';')
                    .includes(r.name)
            )

        const cmdOverDream = message.cleanContent.match(/^!ov?e?r? ?dr?e?a?m?/i)
        if (cmdOverDream) {
            if (checkNoBackendAlive(message)) return
            const msg = message.cleanContent.replace(/^!ov?e?r? ?dr?e?a?m? ?/i, '')
            const width = getInt(msg, /(--width|-W) ([0-9]+)/, 512, 32, 1024, 2)
            const height = getInt(msg, /(--height|-H) ([0-9]+)/, 512, 32, 1024, 2)
            const steps = getInt(msg, /(--steps|-s) ([0-9]+)/, 35, 1, 250, 2)
            const seed = getInt(msg, /(--seed|-S) ([0-9]+)/, Math.floor(Math.random() * 1000000), 1, 1000000000, 2)
            const cfgScale = getFloat(msg, /(--cfg_scale|--scale|-C) ([.0-9]+)/, 7.5, 0, 20, 2)
            const strength = getFloat(msg, /(--strength|-str) ([.0-9]+)/, 0.7, 0, 1, 2)
            const iterations = getInt(msg, /(--iterations|-i) ([0-9]+)/, 0, 0, canBatch ? 4 : 1, 2)
            const dmCopy = !!msg.match(/(-?-dm)/i)
            const normalize = !!msg.match(/(-norm|--normalize)/i)
            const src = msg.match(/-?-src ([^ ]+)/)?.[1]

            let prompt = parsePrompt(msg
                .replace(/(--width|-W) ([0-9]+)/, '')
                .replace(/(--height|-H) ([0-9]+)/, '')
                .replace(/(--steps|-s) ([0-9]+)/, '')
                .replace(/(--seed|-S) ([0-9]+)/, '')
                .replace(/(--cfg_scale|--scale|-C) ([.0-9]+)/, '')
                .replace(/(--strength|-str) ([.0-9]+)/, '')
                .replace(/(--iterations|-i) ([0-9]+)/, '')
                .replace(/(-?-dm)/i, '')
                .replace(/-?-src ([^ ]+)/, '')
                .replace(/(-norm|--normalize)/i, '')
                .replace(/"/i, '')
                .replace(/  +/g, ' ')
                .trim())

            if (prompt.length && prompt.length === 1) prompt = prompt[0]

            const obj = {
                type: "img2img",
                width,
                height,
                seed,
                steps: normalize ? steps : steps,
                scale: cfgScale,
                strength,
                src,
                prompt,
                message,
                iterations: iterations ? iterations : null,
                dmCopy
            }

            const attachment = message.attachments.first()

            if (attachment?.url) obj.src = attachment.url
            queue.push(obj)
            return
        }

        const cmd = message.cleanContent.match(/^!dream/i)
        if (cmd) {
            if (checkNoBackendAlive(message)) return
            const msg = message.cleanContent.replace(/^!dream /i, '')
            const width = getInt(msg, /(--width|-W) ([0-9]+)/, 512, 32, 1024, 2)
            const height = getInt(msg, /(--height|-H) ([0-9]+)/, 512, 32, 1024, 2)
            const steps = getInt(msg, /(--steps|-s) ([0-9]+)/, 35, 1, 250, 2)
            const seed = getInt(msg, /(--seed|-S) ([0-9]+)/, Math.floor(Math.random() * 1000000), 1, 1000000000, 2)
            const iterations = getInt(msg, /(--iterations|-i) ([0-9]+)/, 0, 0, canBatch ? 4 : 1, 2)
            const dmCopy = !!msg.match(/(-?-dm)/i)
            const cfgScale = getFloat(msg, /(--cfg_scale|--scale|-C) ([.0-9]+)/, 7.5, 0, 20, 2)

            let prompt = parsePrompt(msg
                .replace(/(--width|-W) ([0-9]+)/, '')
                .replace(/(--height|-H) ([0-9]+)/, '')
                .replace(/(--steps|-s) ([0-9]+)/, '')
                .replace(/(--seed|-S) ([0-9]+)/, '')
                .replace(/(--cfg_scale|--scale|-C) ([.0-9]+)/, '')
                .replace(/(--iterations|-i) ([0-9]+)/, '')
                .replace(/(-?-dm)/i, '')
                .replace(/"/i, '')
                .replace(/  +/g, ' ')
                .trim())

            if (prompt.length && prompt.length === 1) prompt = prompt[0]

            const obj = {
                type: "txt2img",
                seed,
                width,
                height,
                steps,
                scale: cfgScale,
                prompt,
                message,
                iterations: iterations ? iterations : null,
                dmCopy
            }

            queue.push(obj)
        }
    })

    client.on('warn', async (warning) => {
        console.warn(warning)
    })

    client.on('error', async (error) => {
        console.error(error)
    })

    processQueue(infoChannels).catch(console.error)
    setInterval(async () => {
        const bes = [...backends]
        for (let be of bes) {
            const isAlive = await isBackendAlive(be.url, 1000 * 60 * 5)
            if (!isAlive) {
                await killBackend(infoChannels, be.url)
            }
        }
    }, 1000 * 60 * 10)
})


client.login(process.env.DISCORD_TOKEN).then(r => null)


